package com.bigdatabowl

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object BigDataBowlApp extends App {

  override def main(args: Array[String]): Unit = {
    try {
      val appName = "BigDataBowl Data Util"
      val conf = new SparkConf().setMaster("local[2]")
      val spark = SparkSession.builder().config(conf).appName(appName).getOrCreate()

      val bigDataBowlExecutor: BigDataBowlExecutor = new BigDataBowlExecutor;
//      bigDataBowlExecutor.testCsv()

      bigDataBowlExecutor.coalesceGameCSV("C:\\Users\\mikeb\\IdeaProjects\\BigDataBowl\\game_data")
    }
    catch {
      case x: Exception => {
        x.printStackTrace()
        throw x
      }
    }

  }

}
