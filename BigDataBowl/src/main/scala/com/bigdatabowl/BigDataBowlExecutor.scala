package com.bigdatabowl

import java.io.File

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._

class BigDataBowlExecutor() {

  val spark = SparkSession.builder().getOrCreate()

  import spark.implicits._

  def testCsv(): Unit = {
    val file = "C:\\Users\\mikeb\\IdeaProjects\\BigDataBowl\\data\\game_tracking\\tracking_gameId_2017090700.csv"
    val dir = "C:\\Users\\mikeb\\IdeaProjects\\BigDataBowl\\data\\game_tracking\\"
    val f = new File(dir)
    val files = f.listFiles().filter(_.isFile)
    val fileNames = files.map(f => {
        f.getAbsolutePath
    } ).toSeq
    fileNames.foreach(f => println(f))
    val df = spark.read.option("header", "true").csv(file)
//    val df = spark.read.option("header", "true").csv(file)
//    println(df.show(false))
//    df.printSchema()
  }

  def coalesceGameCSV(dir: String): Unit = {
    val dir = "C:\\Users\\mikeb\\IdeaProjects\\BigDataBowl\\data\\game_tracking\\"
    val f = new File(dir)
    val files = f.listFiles().filter(_.isFile)
    val fileNames = files.map(f => {
      f.getAbsolutePath
    } ).toSeq

    val first = fileNames(0)
    var df = spark.read.option("header", "true").csv(first)
    fileNames.foreach(file => {
      if(!file.equalsIgnoreCase(first))
        df = df.union(spark.read.option("header", "true").csv(file))

    })

    println(df.printSchema())
  }


}
