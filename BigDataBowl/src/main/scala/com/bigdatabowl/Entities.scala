package com.bigdatabowl

import java.sql.Timestamp

case class GameData(time: Timestamp,
                    x: Double,
                    y: Double,
                    s: Double,
                    dis: Double,
                    dir: Double,
                    event: String,
                    nflId: Long,
                    displayName: String,
                    jerseyNumber: Int,
                    team: String,
                   `frame.id`: Int,
                    gameId: Long,
                    playId: Int)
